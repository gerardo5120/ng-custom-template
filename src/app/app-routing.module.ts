import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuard } from './guards/auth.guard'; 

import { EditorComponent } from './components/editor/editor.component'; 
import { LoginComponent } from './components/login/login.component'; 


const routes: Routes = [ 
	{ path: '', redirectTo: '/home', pathMatch: 'full' }, 
	{ path: 'home', component: EditorComponent, canActivate: [ AuthGuard ] }, 
	{ path: 'login', component: LoginComponent }
]; 

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
