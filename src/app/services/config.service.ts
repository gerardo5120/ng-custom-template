import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http'; 
import { environment } from './../../environments/environment'; 
import { AppConfig } from './../interfaces/app-config'; 


@Injectable({
  providedIn: 'root'
})
export class ConfigService {
	static settings: AppConfig; 

	constructor(private http: HttpClient) { /* NOPE */ } 

    load() { 
      return new Promise<void>((resolve, reject) => {
        
        let onError = (fileName: string, response: any) => {
          reject(`Could not load file '${fileName}': ${JSON.stringify(response)}`);
        }; 

        let onDone = (jsonConf: any) => { 

          ConfigService.settings = <AppConfig>jsonConf; 

          if (!environment.name) { 
            resolve(); 
            return; 
          }

          const envFileName = `assets/configs/config.${environment.name}.json`; 
          this.getJson(envFileName, 
            (jsonEnv: any) => { 
              var env = <AppConfig>jsonEnv; 
              ConfigService.settings = Object.assign({}, ConfigService.settings, env); 
              resolve(); 

            }, (fileName, response) => { 
              console.log(response); 
              resolve(); 
            }); 
        }

        const configFileName = `assets/configs/config.json`; 
        this.getJson(configFileName, onDone, onError); 
      }); 
    } 


    private getJson(fileName: string, 
      onRead: (json: any) => void, 
      onError?: (fileName: string, response: any) => void) {

      this.http.get(fileName, { 
        headers: new HttpHeaders({ 'cache-control': 'no-cache' }) }).toPromise().
      then(
        (response: HttpResponse<any>) => onRead(response)). 
      catch( 
        (response: any) => onError(fileName, response)); 

    }

}
