import { Injectable } from '@angular/core'; 
import { Observable, of, throwError } from 'rxjs'; 
import { HttpClient, HttpHeaders } from '@angular/common/http'; 
import { catchError, tap, map, debounceTime } from 'rxjs/operators'; 
import { JwtHelperService } from '@auth0/angular-jwt'; 
import decode from 'jwt-decode'; 
import { Subject } from 'rxjs'; 

import { ConfigService } from './config.service'; 
import { AuthRequest } from './../models/auth-request'; 
import { AuthResponse } from './../models/auth-response'; 


@Injectable({
  providedIn: 'root'
})
export class AuthService {
	public expired = new Subject<any>(); 
	public conf = ConfigService.settings; 

	constructor(private jwtHelper: JwtHelperService, 
		private http: HttpClient) { /* NOPE */ }

	/* public get tokenPayload(): { 
		unique_name: string, role: Array<string>, nbf: number, exp: number, iat: number
	} {
		return decode(localStorage.getItem('token')); 
	} */

	public get isAuthenticated(): boolean { 
		const token = localStorage.getItem('token'); 
		if (!token) { return false; } 

		if (this.jwtHelper.isTokenExpired(token)) { 
			return false; 
		} else { 
			return true; 
		} 
	} 

	public get token(): any { 
		const token = localStorage.getItem('token'); 
		if (!token) { return null; } 
		return this.jwtHelper.decodeToken(token); 
	} 

	public get userName(): any { 
		return this.token ? this.token.sub : ''; 
	} 

	public deleteToken(): any { 
		if (localStorage.getItem('token') == null) { return; } 
		localStorage.removeItem('token'); 
	}

	public authenticate(data:AuthRequest): Observable<AuthResponse> { 
		const url =  `${this.conf.vars.apiUrl}/authenticate`; 

		return this.http.post<AuthResponse>(url, data).pipe(
			tap(_ => console.log(`authenticating for user ${data.username} with password ${data.password}`)), 
			tap(_ => localStorage.setItem('token', _.jwt)), 
			catchError(err => throwError(err))
		); 
	} 
} 
