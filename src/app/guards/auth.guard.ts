import { Injectable } from '@angular/core'; 
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router'; 
import { Observable } from 'rxjs'; 
import { AuthService } from './../services/auth.service'; 


@Injectable({ 
  providedIn: 'root' 
}) 
export class AuthGuard implements CanActivate { 
	constructor(public authSrv: AuthService, 
		public router: Router) { /* NOPE */ } 

	canActivate(): boolean { 
		if (!this.authSrv.isAuthenticated) { 
			sessionStorage.setItem('replyLogin', window.location.href); 
			this.router.navigate(['login']); 
			return false; 
		} else { 
			return true; 
		} 
	} 
} 
