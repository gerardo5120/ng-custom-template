import { Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons'; 

import { BaseComponent } from './../../base/base.component'; 


@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.sass']
})
export class TitleComponent extends BaseComponent implements OnInit { 

  ngOnInit(): void {
  	Feather.replace(); 
  } 

  public delete() { 
  	// NOPE 
  } 

  public copy() { 
  	// NOPE 
  } 

  public otherCommand() { 
  	// this.alert(`Hello World!`, `Mi Titulo`); 
  	this.confirm(`¿Seguro?`, `Mi Titulo`, () => {
  		console.log(`Ejecutando`); 
  	}); 
  } 

}
