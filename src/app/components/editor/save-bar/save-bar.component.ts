import { Component, OnInit } from '@angular/core';
import * as Feather from 'feather-icons'; 

@Component({
  selector: 'app-save-bar',
  templateUrl: './save-bar.component.html',
  styleUrls: ['./save-bar.component.sass']
})
export class SaveBarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  	Feather.replace(); 
  }

}
