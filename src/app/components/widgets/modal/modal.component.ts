import { 
	Component, 
	OnInit, 
	Input, 
	ViewChild, 
	ElementRef, 
	AfterViewInit 
} from '@angular/core'; 
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap'


@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.sass']
})
export class ModalComponent implements OnInit, AfterViewInit { 
	@Input() titleText; 
	@Input() bodyText; 
	@Input() buttons: Array<any> = []; 
	@ViewChild('footer') footer: ElementRef;

  constructor(public activeModal: NgbActiveModal) { /* NOPE */ }

  ngOnInit(): void { 
  	// NOPE 
  } 

  ngAfterViewInit() { 
    let focusBtnIndex = this.buttons.findIndex(x => x.focus == true); 
    if (focusBtnIndex == -1) { return; } 

    setTimeout(() => { 
      this.footer.nativeElement.querySelectorAll('button')[focusBtnIndex].focus(); 
    }, 50); 
  } 

} 
