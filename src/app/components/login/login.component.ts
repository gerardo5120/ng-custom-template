import { Component, OnInit, OnDestroy, Renderer2 } from '@angular/core'; 
import { FormBuilder } from '@angular/forms'; 
import {
  ActivatedRoute, 
  Params, 
  Router 
} from '@angular/router'; 

import {
  HttpClient 
} from '@angular/common/http'; 

import { ConfigService } from './../../services/config.service'; 
import { AuthService } from './../../services/auth.service'; 


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit, OnDestroy { 
  public conf = ConfigService.settings; 
  public loginTicket: string; 
  public checkoutForm; 


  constructor(private activatedRoute: ActivatedRoute, 
    private router: Router, 
    private http: HttpClient, 
    private authSrv: AuthService, 
    private renderer: Renderer2, 
    private formBuilder: FormBuilder) { 

    this.checkoutForm = this.formBuilder.group({
      username: '', 
      password: '' 
    })
  }

  ngOnInit(): void { 
  	this.renderer.addClass(document.body, 'signin'); 
  } 

  ngOnDestroy() { 
  	this.renderer.removeClass(document.body, 'signin'); 
  } 

  public onSubmit(data) { 
    this.authSrv.authenticate(data).subscribe(response => {
      console.log(response); 
      this.router.navigate(['/home']); 
    }); 
  } 

}
