import { Component, OnInit } from '@angular/core'; 
import { AuthService } from './../../services/auth.service'; 
import * as Feather from 'feather-icons'; 


@Component({
  selector: '[app-navbar]',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  constructor(public authSrv: AuthService) { /* NOPE */ }

  ngOnInit(): void { 
  	Feather.replace(); 
  } 

}
