import { Component, OnInit } from '@angular/core'; 
import { ModalComponent } from './../widgets/modal/modal.component'; 
import { AuthService } from '../../services/auth.service'; 
import { 
	NgbActiveModal, 
	NgbModal 
} from '@ng-bootstrap/ng-bootstrap'

@Component({ 
  selector: 'app-base', 
  templateUrl: './base.component.html', 
  styleUrls: ['./base.component.sass'] 
}) 
export class BaseComponent implements OnInit { 

  constructor(public ngbModal: NgbModal, 
  	public authSrv: AuthService) { } 

  ngOnInit(): void {
  }

  public alert(text: string, title: string = 'Atención', onAccept: () => void) { 
    let modal = this.ngbModal.open(ModalComponent, { backdrop: true }); 
    modal.componentInstance.titleText = title; 
    modal.componentInstance.bodyText = text; 
    modal.componentInstance.buttons = [ 
      { 
        text: 'Aceptar', 
        callback: (activeModal: NgbActiveModal) => { 
        	if (onAccept) { 
        		onAccept.call(this); 
        	} 
          	activeModal.close(); 
        } 
      } 
    ]; 
  } 

  public confirm(text: string, title: string, onAccept: () => void) { 
    let modal = this.ngbModal.open(ModalComponent, { backdrop: true }); 
    modal.componentInstance.titleText = title; 
    modal.componentInstance.bodyText = text; 


	modal.componentInstance.buttons.push({ 
		text: 'Cancelar', 
		callback: (activeModal: NgbActiveModal) => { 
		  activeModal.close(); 
		} 
	}); 

    modal.componentInstance.buttons.push({ 
      text: 'Aceptar', 
      callback: (activeModal: NgbActiveModal) => { 
        onAccept.call(this); 
        activeModal.close(); 
      } 
    }); 
  } 

}
