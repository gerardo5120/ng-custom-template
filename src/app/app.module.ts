import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 
import { HttpClientModule } from '@angular/common/http'; 
import { JwtModuleOptions, JwtModule } from '@auth0/angular-jwt'; 

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { EditorComponent } from './components/editor/editor.component';

import { ConfigService } from './services/config.service';
import { SaveBarComponent } from './components/editor/save-bar/save-bar.component';
import { TitleComponent } from './components/editor/title/title.component';
import { DocumentComponent } from './components/editor/document/document.component';
import { ModalComponent } from './components/widgets/modal/modal.component';
import { BaseComponent } from './components/base/base.component'; 


export function initializeApp(appConfig: ConfigService) { 
    return () => appConfig.load(); 
} 

export function tokenGetter() {
  return localStorage.getItem('token'); 
}

const JWT_MODULE_OPTIONS: JwtModuleOptions = {
  config: {
    tokenGetter: tokenGetter, 
    whitelistedDomains: [ 
      'localhost:4200', 
      'localhost:81', 
      'localhost'
    ], 
    blacklistedRoutes: [ ] 
  }
}

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    EditorComponent,
    SaveBarComponent,
    TitleComponent,
    DocumentComponent,
    ModalComponent,
    BaseComponent
  ], 
  imports: [ 
    BrowserModule, 
    AppRoutingModule, 
    NgbModule, 
    HttpClientModule, 
    JwtModule.forRoot(JWT_MODULE_OPTIONS), 
    FormsModule, 
    ReactiveFormsModule
  ], 
  providers: [ 
    { 
      provide: APP_INITIALIZER, 
      useFactory: initializeApp, 
      deps: [ ConfigService ], 
      multi: true 
    } 
  ], 
  bootstrap: [AppComponent], 
  entryComponents: [ 
    ModalComponent 
  ] 
}) 
export class AppModule { /* NOPE */ } 
