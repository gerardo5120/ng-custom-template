import { Component, AfterViewInit } from '@angular/core'; 
import { AuthService } from './services/auth.service'; 


@Component({ 
  selector: 'app-root', 
  templateUrl: './app.component.html', 
  styleUrls: ['./app.component.sass'] 
}) 
export class AppComponent implements AfterViewInit { 
	constructor(public authSrv: AuthService) { /* NOPE */ }

	ngAfterViewInit() { 
		// NOPE 
	} 
} 
